<h1>About</h1>
<p>Welcome! Noor Meister DIY is a website for collecting Estonian do-it-yourself tutorials. Explore, learn, and teach others!</p>
<h2>Contact</h2>
<ul class="contactlist">
    <li class="gmailicon"><a href="mailto:matis.lepik@gmail.com">matis.lepik@gmail.com</a></li>
    <li class="facebookicon"><a href="http://www.facebook.com">www.facebook.com</a></li>
    <li class="twittericon"><a href="http://www.twitter.com">www.twitter.com</a></li>
    <li class="youtubeicon"><a href="http://www.youtube.com">www.youtube.com</a></li>
</ul>