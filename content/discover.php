<?php 
    if (isset($_GET["category"])) {
        $cat = $_GET["category"];
        include $_SERVER['DOCUMENT_ROOT']."/content/discover_{$cat}.php";
    } 
    else {
        include $_SERVER['DOCUMENT_ROOT']."/content/discover_nocat.php";
    }
?>