<div class="register">
    <h1>Register</h1>
    <p class="formMsg"></p>
    <form class="registerform">
        <label>
            <span class="fa fa-user fa-fw"></span>
            <input type="text" placeholder="Username" name="user" autofocus />
        </label>
        <label>
            <span class="fa fa-envelope fa-fw"></span>
            <input type="email" placeholder="E-mail" name="email" />
        </label>
        <label>
            <span class="fa fa-lock fa-fw"></span>
            <input type="password" placeholder="Password" name="pw" />
        </label>
        <label>
            <span class="fa fa-lock fa-fw"></span>
            <input type="password" placeholder="Password (again)" name="pw2" />
        </label>
        <div class="clickable button" id="registerbutton">Register</div>
    </form>
</div>