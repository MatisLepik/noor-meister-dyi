<?php
    require_once("globals.php");

    if (preg_match("/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i", $_POST['email'])) {
        try {
            $sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME, DB_USERNAME, DB_PASSWORD);
            $query = $sql->prepare("SELECT username FROM users WHERE username = ?");
            $query -> execute(array($_POST['username']));
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($result) {
                echo "Username already exists!";
            } else {
                createAccount($sql);
            }
            
        } catch (PDOException $e) {
            echo $e;
        } 
    } else {
        echo "Invalid email address.";
    }

    function createAccount($sql) {
        $hashedpw = password_hash($_POST['password'], PASSWORD_BCRYPT);
        
        try {
            $query = $sql->prepare("INSERT INTO users (username, email, password) VALUES (:username, :email, :password)");
            $query -> bindParam(':username', $_POST['username']); 
            $query -> bindParam(':email', $_POST['email']); 
            $query -> bindParam(':password', $hashedpw); 
            if ($query -> execute()) {
                echo "true";
            } else {
                echo json_encode($query -> errorInfo()[2]);
            }
        } catch (PDOException $e) {
            echo $e;
        }
    }
?>