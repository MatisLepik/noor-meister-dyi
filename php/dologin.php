<?php
    session_start();
    require_once('globals.php');

    try {
        $sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME, DB_USERNAME, DB_PASSWORD);
        $query = $sql -> prepare("SELECT password, username FROM users WHERE username=:username");
        $query -> bindParam(":username", $_POST['username']);
        $query -> execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        
        if ($results) {
            $hashedpw = $results[0]['password'];
            if (password_verify($_POST['password'], $hashedpw)) {
                $_SESSION["user"] = $results[0]['username'];
                echo "true";
            } else {
                echo "false";
            }
        } else {
            echo "false";
        }
    } catch (PDOException $e) {
        echo $e;
    }

?>