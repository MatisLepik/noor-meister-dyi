<?php
    
    $file = getFileName($_GET["page"]);
    if (file_exists($file)) {
        include $file;
    } else {
        include getFileName("discover");
    }

    function getFileName($path) 
    {
        //Remove trailing '/' if one exists
        if (substr($path, strlen($path) - 1) == "/") {
            $path = substr($path, 0, strlen($path) - 1);
        }
        //Add leading '/' if one doesn't exists
        if (substr($path, 0) !== "/") {
            $path = "/".$path;
        }
       return $_SERVER['DOCUMENT_ROOT']."/content{$path}.php";
    }
?>