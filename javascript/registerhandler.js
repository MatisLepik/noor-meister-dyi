var attemptingRegister = false;

function initRegisterHandler() {
    $(document).on('click', '#registerbutton', function () {
        register($('.registerform input[name="user"]')[0].value,
            $('.registerform input[name="email"]')[0].value,
            $('.registerform input[name="pw"]')[0].value,
            $('.registerform input[name="pw2"]')[0].value
        );
    });
}

function register(user, email, pw, pw2) {
    if (user == "" || email == "" || pw == "" || pw2 == "") {
        displayMsg("Please fill all the fields!", '#e22a40');
    } else if (/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i.test(email) == false) {
        displayMsg("Invalid email address!", '#e22a40');
    } else if (pw !== pw2) {
        displayMsg("Password do not match!", '#e22a40');
    } else {
        if (!attemptingRegister) {
            attemptingRegister = true;

            $.post('/php/doregister.php', {
                username: user,
                email: email,
                password: pw
            }).done(function (data) {
                attemptingRegister = false;

                if (data === "true") {
                    displayMsg("Registration successful! Welcome to the site, " + user, "#227ed3");
                } else {
                    displayMsg(data, '#e22a40');
                }
            });
        }
    }

}

function displayMsg(msg, color) {
    color = typeof color === 'undefined' ? 'black' : color;
    $('.register .formMsg').html(msg).css('color', color).slideDown(animSpeed * 2);
}