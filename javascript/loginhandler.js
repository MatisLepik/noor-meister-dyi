var attemptingLogin = false;
var userField = $('.loginbox input[name="username"]')[0];
var pwField = $('.loginbox input[name="password"]')[0];

function initLoginHandler() {
    $('#navilogin').on('click', toggleOverlay);
    $('.loginoverlay').on('click', toggleOverlay);
    $('.loginbox > .close').on('click', toggleOverlay);
    $('.loginbox').on('click', function (evt) {
        evt.stopPropagation();
    });

    $('#loginbutton').on('click', logIn);
    $('#logoutbutton').on('click', logOut);

}

function logIn() {
    resetInputStyle();
    if (!attemptingLogin) {
        if (userField.value === "") loginFail();
        else if (pwField.value === "") loginFail();
        else {
            attemptingLogin = true;

            $.post('/php/dologin.php', {
                username: userField.value,
                password: pwField.value
            }).done(function (data) {
                attemptingLogin = false;
                if (data === "true") {
                    location.reload();
                } else {
                    loginFail();
                }
            });
        }
    }
}

function loginFail() {
    $('#loginbutton').animate({
        backgroundColor: "#e22a40"
    }, {
        duration: animSpeed,
        queue: false,
        done: function () {
            $('#loginbutton').animate({
                backgroundColor: "#515151"
            }, {
                duration: animSpeed * 2,
                queue: false
            })
        }
    })
    userField.value = "";
    pwField.value = "";
}

function logOut() {
    $.post('/php/dologout.php').done(function (data) {
        location.reload();
    });
}

function resetInputStyle() {
    $('.loginbox input').removeClass('failedInput');
}

function toggleOverlay() {
    resetInputStyle();
    var el = $('.loginoverlay');
    el.fadeToggle(200);
}