var currentPage;
var loadingPage = false;
var defaultPage = "discover/";

function initPageHandler() {
    $('.content').css('opacity', '0'); //Gets animated back to 1

    $('.navibar ul').on('click', function (evt) {
        evt.stopPropagation();
    });

    $('.navibar ul li[id!="navilogin"]').on('click', naviClick);

    //Collapses submenu when submenu item is clicked
    $('.navibar ul ul > li').on('click', function (evt) {
        $('.navibar ul ul').hide(1).delay(300).show(1);
    });

    //Handles back/forward browser functionality
    $(window).on('popstate', function (evt) {
        popPage(evt.originalEvent.state.event);
    });
    
    $('#registerlink').on('click', function (evt) {
        naviClick(evt);
        toggleOverlay(); //loginhandler.js
    });
    
    processPage();
}

/**
 * Creates a logical page string in the form "/page1/page2/?field1=value1&field2=value2"
 * @param {string} page - form "/page1/page2/"
 * @param {string[]|string} params [optional] - accepts: A string ("?field1=1&field2=2) or an array (["field1=1","field2=2"])
 */
function LogicalPage(page, params) {
    this.page = typeof page === "string" ? page : defaultPage;

    switch (typeof params) {
    case "string":
        if (params.charAt(0) !== "?") params = "?" + params;
        this.params = params;
        break;
    case "object":
        this.params = typeof params === "object" ? "?" + params.join("&") : "";
        break;
    default:
        this.params = "";
        break;
    }
}
LogicalPage.prototype.getPage = function () {
    return this.page + this.params;
};

function processPage() {
    popPage(new LogicalPage(window.location.pathname, window.location.search));
}

function naviClick(evt) {
    evt.stopPropagation();
    var page = $(evt.target).data('page');
    var cat = $(evt.target).data('cat');
    var params = typeof cat !== 'undefined' ? "category=" + cat : false;

    openPage(new LogicalPage(page, params));

}

/** @param {LogicalPage} logicalPage */

function popPage(logicalPage) {
    if (loadingPage) return false;

    loadingPage = true;
    $.get('/php/getcontent.php' + logicalPage.params, {
        page: logicalPage.page
    }).done(function (data) {
        updateContent(data);
        currentPage = logicalPage;
        updateTitle(logicalPage);
        updateNavi(logicalPage);
        loadingPage = false;
    });
}

function openPage(logicalPage) {
    popPage(logicalPage);
    updateAddress(logicalPage);
}

/** @param {String} html data */
function updateContent(data) {
    //Fade content out, then load new content and fade back in
    $('.content').animate({
        opacity: '0',
        top: '-10px'
    }, {
        duration: animSpeed,
        queue: false,
        done: function () {
            $('.content').html(data);
            $('.content').animate({
                opacity: '1',
                top: '0'
            }, {
                duration: animSpeed,
                queue: false
            });
        }
    });
}

function updateAddress(logicalPage) {
    window.history.pushState({
        event: logicalPage
    }, "Loading title..", logicalPage.getPage());
}

function updateTitle(logicalPage) {
    //Format and set title (capitalization + replaces "/" with " > ")
    var title = logicalPage.page.substr(0, logicalPage.page.length - 1).toLowerCase().replace(/\//g, " > ").replace(/>../g, function (match) {
        return match.toUpperCase();
    });
    document.title = 'Noor Meister DIY' + title;
}

function updateNavi(logicalPage) {
    //Clear navigation's activepage if it has any
    $('.navibar li').removeClass('activepage');

    //Tries to find the corresponding navigation element and add activepage class. If it doesn't exist (i.e we've linked to a page that's not a part of the navi), nothing happens
    var splitPage = logicalPage.page.split('/');
    $('.navibar > ul > li[data-page="/' + splitPage[splitPage.length - 2] + '/"]').addClass('activepage');
}

function getParam(query, field) {
    field = field.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + field + "=([^&#]*)"),
        results = regex.exec(query);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}