~Unfinished~

# README #

Noor Meister DIY by Matis Lepik

This is a website I made as part of practice for the Noor Meister competition in Estonia. In short, the assignment was: Create a DIY tutorial website where users can browse and submit their own tutorials. 

My solution is a single-page web application that uses AJAX fetch data and JavaScript's History API to achieve a nice clean address bar. 

Languages used: HTML, CSS, JavaScript (with jQuery), PHP, MySQL

**You can see the [live version](http://diy.matislepik.eu/) on my website.**

# SETUP #

Put the project the root directory of an Apache server. The site relies on .htaccess to redirect every request to the index.php file, the routing is handled in JavaScript/PHP. 
Page can be set up to work in a subdirectory with some code modifications.

Database setup: If running for the first time, copy the mysql code from initdb.txt and run it in mysql.

# CONTACT #

matis.lepik@gmail.com