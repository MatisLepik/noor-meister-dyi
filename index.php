<?php 
    session_start();
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Share your DIY tutorials via Noor Meister DIY">
    <meta name="keywords" content="DIY, NoorMeister">
    <meta name="author" content="Matis Lepik">
    <title>Noor Meister DIY</title>

    <link rel='stylesheet' href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Amaranth|Open+Sans:400,600' type='text/css'>
    <link rel='stylesheet' href="/css/font-awesome.min.css">
    <link rel='stylesheet' href="/css/style.css">
    <link rel='icon' href="/img/favicon.ico">
    <link rel='image_src' href="/img/logo.png">

</head>

<body>
    <div class="pagewrapper">
        <header>
            <img src="/img/logo.png" alt="DIY Logo" width="292" height="144">
        </header>
        <div class="content">
            <p>Loading...</p>
        </div>
        <footer>Copyright &copy;
            <?php echo date( 'Y'); ?>| matis.lepik@gmail.com</footer>
    </div>
    <div class="navibar">
        <ul>
            <li class="clickable" data-page="/discover/">Discover
                <div class="downarrow"></div>
                <ul>
                    <li class="clickable" data-page="/discover/" data-cat="home">Home</li>
                    <li class="clickable" data-page="/discover/" data-cat="crafting">Crafting</li>
                    <li class="clickable" data-page="/discover/" data-cat="reusage">Reusage</li>
                    <li class="clickable" data-page="/discover/" data-cat="technology">Technology</li>
                    <li class="clickable" data-page="/discover/" data-cat="food">Food</li>
                    <li class="clickable" data-page="/discover/" data-cat="freetime">Free time</li>
                </ul>
            </li>
            <li class="clickable" data-page="/teach/">Teach</li>
            <li class="clickable" data-page="/about/">About</li>
            <?php if (empty($_SESSION["user"])) {
                    echo "<li class='clickable' id='navilogin'>Login / Register</li>";
                } else {
                    echo "<li class='clickable' id='logoutbutton'>Log out {$_SESSION["user"]}</li>";
                } 
            ?>
        </ul>
    </div>
    <div class="loginoverlay">
        <div class="lightbox"></div>
        <div class="loginbox">
            <div class="close clickable">X</div>
            <form>
                <input type="text" placeholder="Username" name="username" autofocus />
                <input type="password" placeholder="Password" name="password" />
                <div class="clickable button" id="loginbutton">Log in</div>
                <a href="javascript:void(0)" id="registerlink" data-page="/register/">Don't have an account? Register here!</a>
            </form>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://code.jquery.com/color/jquery.color-2.1.2.min.js"></script>
    <script src="/javascript/registerhandler.js"></script>
    <script src="/javascript/pagehandler.js"></script>
    <script src="/javascript/loginhandler.js"></script>
    <script src="/javascript/scripts.js"></script>
</body>

</html>