CREATE DATABASE noormeisterdiy;
USE noormeisterdiy;

CREATE TABLE users (
id INT AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(40) UNIQUE NOT NULL,
email VARCHAR(255) UNIQUE NOT NULL,
password VARCHAR(255) NOT NULL,
registered TIMESTAMP,
status CHAR(1) NOT NULL DEFAULT 'u'
);
-- Creates an admin with password of "1234"
INSERT INTO users (username, email, password, status) VALUES ("admin", "admin@localhost", "$2y$10$vDWNkSuX4PEA4vo.qe.THedJCoTOQA.QBq7iZE2rxdG7N35WujQfC", "a");

CREATE USER 'diyAdmin' IDENTIFIED BY 'testpw';
GRANT ALL PRIVILEGES ON *.* TO 'diyAdmin';